using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheatBot;

public static class Utilities
{
    private static readonly char[] Separators = [' ', '_'];

    internal static uint GetRandomNumber()
	{
		uint number = 0;
		while ((number == 0) || Program.Database.QuestionSets.ContainsKey(number))
		{
#pragma warning disable CA5394
			number = (uint) Random.Shared.Next(int.MinValue, int.MaxValue);
#pragma warning restore CA5394
		}

		return number;
	}

	internal static string LimitStringLength(this string input, int length) => input.Length > length ? input.Remove(length - 1) + "…" : input;

	internal static string NormalizeStringForIndex(string input) => string.Join(' ', new string(input.Select(titleChar => char.IsLetter(titleChar) ? titleChar : ' ').ToArray()).Split(' ', StringSplitOptions.RemoveEmptyEntries));

	internal static string[] ParseArguments(string input)
	{
		var args = new List<string>();
		var isEscaped = false;
		var temporary = new StringBuilder();
		var splitInput = input.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
		foreach (var word in splitInput)
		{
			if (!isEscaped)
			{
				if (word[0] != '"')
				{
					args.Add(word);
				} else
				{
					var tempWord = word.Substring(1);
					if (tempWord[^1] == '"')
					{
						args.Add(tempWord.Remove(tempWord.Length - 1));
					} else
					{
						isEscaped = true;
						temporary.Append(tempWord + ' ');
					}
				}
			} else
			{
				if (word[^1] != '"')
				{
					temporary.Append(word + ' ');
				} else
				{
					isEscaped = false;
					temporary.Append(word.AsSpan(0, word.Length - 1));
					args.Add(temporary.ToString());
					temporary = new StringBuilder();
				}
			}
		}

		if (temporary.Length > 0)
		{
			var temp = temporary.ToString().TrimEnd();
			args.Add(temp);
		}

		return args.ToArray();
	}
}
