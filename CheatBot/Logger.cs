using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace CheatBot;

internal sealed class Logger
{
#if DEBUG
	private const bool debugging = true;
#else
	private const bool debugging = false;
#endif

	internal Logger(string loggerName) => _loggerName = loggerName;

	private static readonly object LockFile = new();
	private readonly string _loggerName;

	internal void Info(string text, [CallerMemberName] string? method = null)
	{
		Log(string.IsNullOrEmpty(_loggerName) ? text : _loggerName + "|" + text, Level.Info, method);
	}

	internal void Error(string text, [CallerMemberName] string? method = null)
	{
		Log(string.IsNullOrEmpty(_loggerName) ? text : _loggerName + "|" + text, Level.Error, method);
	}

	internal void Debug(string text, [CallerMemberName] string? method = null)
	{
		Log(string.IsNullOrEmpty(_loggerName) ? text : _loggerName + "|" + text, Level.Debug, method);
	}

	private static void Log(string text, Level level, string? method)
	{
		var stringToLog = $"[{DateTime.Now:G}]|{Enum.GetName(level)!.ToUpperInvariant()}|{method}|{text}";
		Console.ForegroundColor = level switch
		{
			Level.Info => ConsoleColor.White,
			Level.Error => ConsoleColor.Red,
			Level.Debug => ConsoleColor.Gray,
			_ => Console.ForegroundColor
		};

		if ((level != Level.Debug) || debugging)
		{
			Console.WriteLine(stringToLog);
		}

		lock (LockFile)
		{
			File.AppendAllText("log.txt", stringToLog + Environment.NewLine);
		}
	}

	private enum Level
	{
		Info,
		Error,
		Debug
	}
}
