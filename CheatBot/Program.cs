﻿using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CheatBot;

internal static class Program
{
	internal static readonly SemaphoreSlim ShutdownSemaphore = new(0, 1);
	private static readonly Logger ProgramLogger = new(nameof(Program));

	internal static Configuration Config { get; private set; } = null!;
	internal static Database Database { get; private set; } = null!;

	private static BotInstance BotInstance { get; set; } = null!;

	private static async Task Main()
	{
		ProgramLogger.Info("Starting...");
		CultureInfo.CurrentCulture = new CultureInfo("ru-RU");

		if (!Directory.Exists("indexes"))
		{
			Directory.CreateDirectory("indexes");
		}

		ProgramLogger.Debug("Loading config...");
		var loadedConfig = Configuration.Load();
		if (loadedConfig == null)
		{
			ProgramLogger.Debug("Config not found, creating...");
			loadedConfig = Configuration.Create();
			loadedConfig.Save();
		}

		Config = loadedConfig;

		var loadedDatabase = Database.Load();
		if (loadedDatabase == null)
		{
			ProgramLogger.Debug("Database not found, creating...");
			loadedDatabase = Database.Create();
			loadedDatabase.Save();
		}

		Database = loadedDatabase;

		BotInstance = new BotInstance(nameof(CheatBot));
		if (!await BotInstance.Start())
		{
			ProgramLogger.Error("Невозможно запустить бота!");

			return;
		}

		ProgramLogger.Info("Бот успешно запущен!");
		await ShutdownSemaphore.WaitAsync();

		BotInstance.Stop();
		BotInstance.Dispose();
		Database.Dispose();
		ShutdownSemaphore.Release();
		ShutdownSemaphore.Dispose();
		foreach (var questionSet in Database.QuestionSets.Values)
		{
			questionSet.Dispose();
		}
	}
}
