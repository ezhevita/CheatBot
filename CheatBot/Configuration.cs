using System;
using System.IO;
using Newtonsoft.Json;

namespace CheatBot;

internal sealed class Configuration
{
	[JsonConstructor]
	private Configuration()
	{
	}

	[JsonProperty]
	internal string BotToken { get; private set; } = null!;

	internal static Configuration Create()
	{
		var config = new Configuration();

		string? input = null;
		while (string.IsNullOrEmpty(input))
		{
			Console.WriteLine(nameof(BotToken) + ":");
			input = Console.ReadLine();
		}

		config.BotToken = input;
		return config;
	}

	internal static Configuration? Load()
	{
		if (!File.Exists("config.json"))
		{
			return null;
		}

		var content = File.ReadAllText("config.json");
		Configuration? loadedConfig;
		try
		{
			loadedConfig = JsonConvert.DeserializeObject<Configuration>(content);

			if (loadedConfig == null)
			{
				return null;
			}
		} catch (JsonException)
		{
			return null;
		}

		if (string.IsNullOrEmpty(loadedConfig.BotToken))
		{
			string? input = null;
			while (string.IsNullOrEmpty(input))
			{
				Console.WriteLine(nameof(BotToken) + ":");
				input = Console.ReadLine();
			}

			loadedConfig.BotToken = input;
			loadedConfig.Save();
		}

		return loadedConfig;
	}

	internal void Save()
	{
		var content = JsonConvert.SerializeObject(this, Formatting.Indented);
		File.WriteAllText("config.json", content);
	}
}
