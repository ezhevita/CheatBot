using System;
using System.Globalization;
using System.IO;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Util;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Newtonsoft.Json;
using Directory = System.IO.Directory;

namespace CheatBot;

internal sealed class QuestionSet : IDisposable
{
	private static readonly StandardAnalyzer Analyzer = new(LuceneVersion.LUCENE_48, CharArraySet.EMPTY_SET);

	private static readonly IndexWriterConfig WriterConfig = new(LuceneVersion.LUCENE_48, Analyzer)
	{
		OpenMode = OpenMode.CREATE_OR_APPEND
	};

	private readonly FSDirectory _indexFolder;
	private readonly IndexWriter _indexWriter;

	[JsonConstructor]
	internal QuestionSet(uint id, long creatorID)
	{
		ID = id;
		CreatorID = creatorID;
		var indexPath = Path.Join(Directory.GetCurrentDirectory(), "indexes", ID.ToString(CultureInfo.InvariantCulture));
		if (!Directory.Exists(indexPath))
		{
			Directory.CreateDirectory(indexPath);
		}

		_indexFolder = FSDirectory.Open(indexPath);
		_indexWriter = new IndexWriter(_indexFolder, (IndexWriterConfig) WriterConfig.Clone());
	}

	[JsonProperty]
	internal uint ID { get; private set; }

	[JsonProperty]
	internal long CreatorID { get; private set; }

	[JsonProperty]
	private int IncrementalID { get; set; }

	internal int Count => _indexWriter.NumDocs;

	private DirectoryReader Reader => _indexWriter.GetReader(true);
	internal IndexSearcher Searcher => new(Reader);

	public void Dispose()
	{
		_indexWriter.Commit();
		_indexFolder.Dispose();
		_indexWriter.Dispose();
	}

	internal void Add(string question, string answer, string type = "Text", string fileID = "")
	{
		var document = new Document
		{
			new TextField("questionIndex", Utilities.NormalizeStringForIndex(question), Field.Store.NO),
			new StringField("question", question, Field.Store.YES),
			new StringField("answer", answer, Field.Store.YES),
			new StringField("type", type, Field.Store.YES),
			new StringField("fileID", fileID, Field.Store.YES)
		};

		_indexWriter.AddDocument(document);
	}

	internal bool Delete(int docID) => _indexWriter.TryDeleteDocument(Reader, docID);
}
