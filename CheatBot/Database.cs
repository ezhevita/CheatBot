using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Telegram.Bot.Types;
using File = System.IO.File;

namespace CheatBot;

public sealed class Database : IDisposable
{
	private readonly object _lockSave = new();
	private readonly Timer _saveTimer;

	[JsonProperty]
	internal uint RequestsAnswered;

	[JsonConstructor]
	internal Database()
	{
		_saveTimer = new Timer(_ => Save(), null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));
	}

	[JsonProperty]
	internal ConcurrentDictionary<long, User> KnownUsers { get; private set; } = new();

	[JsonProperty]
	internal ConcurrentDictionary<uint, QuestionSet> QuestionSets { get; private set; } = new();

	[JsonProperty]
	internal ConcurrentDictionary<long, HashSet<uint>> UserSubscriptions { get; private set; } = new();

	public void Dispose()
	{
		Save();
		_saveTimer.Dispose();
	}

	internal static Database Create()
	{
		var database = new Database();

		return database;
	}

	internal static Database? Load()
	{
		if (!File.Exists("data.json"))
		{
			return null;
		}

		var content = File.ReadAllText("data.json");
		Database? loadedDatabase;
		try
		{
			loadedDatabase = JsonConvert.DeserializeObject<Database>(content);

			if (loadedDatabase == null)
			{
				return null;
			}
		} catch (JsonException)
		{
			return null;
		}

		return loadedDatabase;
	}

	internal void Save()
	{
		lock (_lockSave)
		{
			var content = JsonConvert.SerializeObject(this);
			File.WriteAllText("data.json", content);
		}
	}
}
