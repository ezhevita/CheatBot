using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Spans;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace CheatBot;

public sealed class BotInstance : IDisposable
{
	private const long developerID = 204723509;

	private readonly Logger _botLogger;
	private readonly Dictionary<long, (SemaphoreSlim WaitSemaphore, Message? ResultMessage)> _waitMessageStatuses = new();

	private TelegramBotClient _botClient = null!;
	private readonly CancellationTokenSource _cancellationTokenSource = new();

	internal BotInstance(string name) => _botLogger = new Logger(name + "-Bot");

	private async Task HandleUpdate(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
	{
		if (update is {Type: UpdateType.Message, Message.Type: >= MessageType.Text and <= MessageType.Document})
		{
			await BotClientOnMessage(update.Message);
		}
	}

	private async Task BotClientOnMessage(Message message)
	{
		var senderID = message.From!.Id;

		try
		{
			if (Program.Database.KnownUsers.ContainsKey(senderID))
			{
				Program.Database.KnownUsers[senderID] = message.From;
			} else
			{
				Program.Database.KnownUsers.TryAdd(senderID, message.From);
				Program.Database.UserSubscriptions.TryAdd(senderID, new HashSet<uint>());
			}

			if (message.Chat.Type != ChatType.Private)
			{
				return;
			}

			_botLogger.Debug(senderID + "|" + message.Type + "|" + (message.Type == MessageType.Text ? message.Text : message.Caption));
			if (_waitMessageStatuses.TryGetValue(senderID, out var value))
			{
				if (message.Text?.ToUpperInvariant() != "/ABORT")
				{
					_waitMessageStatuses[senderID] = (value.WaitSemaphore, message);
				}

				if (value.WaitSemaphore.CurrentCount == 0)
				{
					value.WaitSemaphore.Release();
				}

				return;
			}

			if (string.IsNullOrEmpty(message.Text))
			{
				return;
			}

			if (message.Text[0] == '/')
			{
				var messageText = message.Text.Substring(1);
				var commandArgs = Utilities.ParseArguments(messageText);

				if (commandArgs.Length == 0)
				{
					return;
				}

				switch (commandArgs[0].ToUpperInvariant())
				{
					case "ADD" when commandArgs.Length == 2:
						await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Separated);

						break;

					case "ADDSINGLE" when commandArgs.Length == 2:
						await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Single);

						break;

					case "ADDMULTIPLE" when commandArgs.Length == 2:
						await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Multi);

						break;

					case "ANNOUNCE" when HasPermission(senderID, EPermission.Admin):
					{
						await _botClient.SendTextMessageAsync(senderID, "Введите текст сообщения для анонса:");
						var announceMessage = await WaitForMessage(senderID);
						if (announceMessage?.Text == null)
						{
							await _botClient.SendTextMessageAsync(senderID, "Отменено.");

							return;
						}

						await _botClient.SendTextMessageAsync(senderID, "Рассылка начата.");
						foreach (var userID in Program.Database.KnownUsers.Keys)
						{
							try
							{
								await _botClient.SendTextMessageAsync(userID, announceMessage.Text);
								await Task.Delay(1000);
							} catch (ApiRequestException)
							{
								// ignored
							}
						}

						await _botClient.SendTextMessageAsync(senderID, "Рассылка закончена.");

						break;
					}

					case "ANSWER" when commandArgs.Length > 1:
					{
						if (!uint.TryParse(commandArgs[1], out var questionSetID) || !Program.Database.QuestionSets.TryGetValue(questionSetID, out var questionSet) || !Program.Database.UserSubscriptions[senderID].Contains(questionSetID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID набора!");

							return;
						}

						if (!int.TryParse(commandArgs[2], out var questionID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID вопроса!");

							return;
						}

						var doc = questionSet.Searcher.Doc(questionID);
						if (doc == null)
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID вопроса!");

							return;
						}

						var type = Enum.Parse<MessageType>(doc.GetField("type").GetStringValue(CultureInfo.InvariantCulture));

						var question = doc.GetField("question").GetStringValue(CultureInfo.CurrentCulture);
						var answer = doc.GetField("answer").GetStringValue(CultureInfo.CurrentCulture);
						switch (type)
						{
							case MessageType.Text:
								await _botClient.SendTextMessageAsync(senderID, question + "\n" + answer);

								break;
							case MessageType.Photo:
								await _botClient.SendPhotoAsync(senderID, new InputFileId(doc.GetField("fileID").GetStringValue(CultureInfo.InvariantCulture)), caption: question + "\n" + answer);

								break;
							case MessageType.Document:
								await _botClient.SendDocumentAsync(senderID, new InputFileId(doc.GetField("fileID").GetStringValue(CultureInfo.InvariantCulture)), caption: question + "\n" + answer);

								break;
							default:
								return;
						}

						break;
					}

					case "CREATE":
					{
						if (Program.Database.QuestionSets.Count(set => set.Value.CreatorID == senderID) > 500)
						{
							await _botClient.SendTextMessageAsync(senderID, "Вы превысили лимит по количеству наборов.");

							return;
						}

						var id = Utilities.GetRandomNumber();
						Program.Database.QuestionSets.TryAdd(id, new QuestionSet(id, senderID));
						Program.Database.UserSubscriptions[senderID].Add(id);
						await _botClient.SendTextMessageAsync(senderID, $"Успешно добавлен набор с ID {id}");

						break;
					}

					case "DELETE" when commandArgs.Length > 2:
					{
						if (!uint.TryParse(commandArgs[1], out var id) || !Program.Database.QuestionSets.TryGetValue(id, out var questionSet) || (questionSet.CreatorID != senderID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID!");

							return;
						}

						if (!int.TryParse(commandArgs[2], out var docID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный docID!");

							return;
						}

						if (questionSet.Delete(docID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Успешно удалено!");
						} else
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный docID!");
						}

						break;
					}

					case "DELETE" when commandArgs.Length > 1:
					{
						if (!uint.TryParse(commandArgs[1], out var id) || !Program.Database.QuestionSets.TryGetValue(id, out var questionSet) || (questionSet.CreatorID != senderID))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID!");

							return;
						}

						await _botClient.SendTextMessageAsync(senderID, "Введите поисковый запрос:");

						var inputMessage = await WaitForMessage(senderID);
						if ((inputMessage == null) || string.IsNullOrEmpty(inputMessage.Text))
						{
							await _botClient.SendTextMessageAsync(senderID, "Отменено.");

							return;
						}

#pragma warning disable CA1308
						var searchArgs = Utilities.NormalizeStringForIndex(inputMessage.Text).ToLowerInvariant().Split(' ', StringSplitOptions.RemoveEmptyEntries);
#pragma warning restore CA1308
						var searcher = Program.Database.QuestionSets[id].Searcher;
						var query = new SpanNearQuery(searchArgs.Select(searchArg => new SpanMultiTermQueryWrapper<FuzzyQuery>(new FuzzyQuery(new Term("questionIndex", searchArg), 2))).Cast<SpanQuery>().ToArray(), 1, false);
						var hits = searcher.Search(query, 5).ScoreDocs;

						var response = string.Join('\n', hits.Select(hit => searcher.Doc(hit.Doc).GetField("question").GetStringValue(CultureInfo.CurrentCulture) + " - /delete_" + id + "_" + hit.Doc));

						await _botClient.SendTextMessageAsync(senderID, string.IsNullOrEmpty(response) ? "Пусто..." : response);

						break;
					}

					case "DONATE":
						await _botClient.SendTextMessageAsync(
							message.Chat.Id, $"Спасибо, что решили задонатить! За всё время бот обработал {Program.Database.RequestsAnswered} запросов, " +
							$"в базе содержится {Program.Database.QuestionSets.Count} наборов вопросов.\n\n" +
							"WebMoney:\n  WMZ: Z028553335407\n" +
							"[Яндекс.Деньги](https://money.yandex.ru/to/410012842075676)\n" +
							"[Ссылка обмена Steam](https://steamcommunity.com/tradeoffer/new/?partner=186729617&token=XvcQ5RHm)\n" +
							"[Связаться с разработчиком](https://t.me/ezhevita)", parseMode: ParseMode.Markdown
						);

						break;

					case "LIST":
					{
						var stringBuilder = new StringBuilder();
						var createdSetIDs = Program.Database.QuestionSets.Where(set => set.Value.CreatorID == senderID).Select(set => set.Key).ToHashSet();
						if (createdSetIDs.Count > 0)
						{
							stringBuilder.Append("Созданные наборы: ");
							stringBuilder.Append(string.Join(", ", createdSetIDs));
							stringBuilder.Append('\n');
						}

						var subscriptions = Program.Database.UserSubscriptions[senderID];
						if (subscriptions.Count > 0)
						{
							stringBuilder.Append("Подписки: ");
							stringBuilder.Append(string.Join(", ", subscriptions));
						}

						var stringBuilderResponse = stringBuilder.ToString();
						var response = string.IsNullOrEmpty(stringBuilderResponse) ? "Пусто..." : stringBuilderResponse;
						await _botClient.SendTextMessageAsync(senderID, response);

						break;
					}

					case "START":
					{
						await _botClient.SendTextMessageAsync(
							senderID, "Добро пожаловать в бота CheatBot! Бот позволяет подготовиться к экзамену (или списать прямо на нём). Разработчик - @ezhevita. Реквизиты для доната - /donate."
						);

						break;
					}

					case "STOP" when HasPermission(senderID, EPermission.Admin):
					{
						Program.ShutdownSemaphore.Release();

						break;
					}

					case "SUB" when commandArgs.Length > 1:
					{
						if (Program.Database.UserSubscriptions[senderID].Count > 100)
						{
							await _botClient.SendTextMessageAsync(senderID, "Вы превысили лимит по количеству подписок.");

							return;
						}

						if (!uint.TryParse(commandArgs[1], out var id) || (id == 0) || !Program.Database.QuestionSets.ContainsKey(id))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID!");

							return;
						}

						if (Program.Database.UserSubscriptions[senderID].Contains(id))
						{
							await _botClient.SendTextMessageAsync(senderID, "Вы уже подписаны на этот набор!");

							return;
						}

						Program.Database.UserSubscriptions[senderID].Add(id);
						await _botClient.SendTextMessageAsync(senderID, "Подписка добавлена.");

						break;
					}

					case "UNSUB" when commandArgs.Length > 1:
					{
						if (!uint.TryParse(commandArgs[1], out var id) || (id == 0) || !Program.Database.QuestionSets.ContainsKey(id))
						{
							await _botClient.SendTextMessageAsync(senderID, "Неверный ID!");

							return;
						}

						if (!Program.Database.UserSubscriptions[senderID].Contains(id))
						{
							await _botClient.SendTextMessageAsync(senderID, "Вы не подписаны на этот набор!");

							return;
						}

						Program.Database.UserSubscriptions[senderID].Remove(id);
						await _botClient.SendTextMessageAsync(senderID, "Подписка удалена.");

						break;
					}
				}
			} else
			{
				if (Program.Database.UserSubscriptions[senderID].Count == 0)
				{
					await _botClient.SendTextMessageAsync(senderID, "У вас нет подписок!");

					return;
				}

				Program.Database.RequestsAnswered++;
				var subscriptions = Program.Database.UserSubscriptions[senderID];

				var totalHits = new List<string>();
#pragma warning disable CA1308
				var searchArgs = Utilities.NormalizeStringForIndex(message.Text).ToLowerInvariant().Split(' ', StringSplitOptions.RemoveEmptyEntries);
#pragma warning restore CA1308
				foreach (var id in subscriptions)
				{
					var searcher = Program.Database.QuestionSets[id].Searcher;
					var query = new SpanNearQuery(searchArgs.Select(searchArg => new SpanMultiTermQueryWrapper<FuzzyQuery>(new FuzzyQuery(new Term("questionIndex", searchArg), 2))).Cast<SpanQuery>().ToArray(), 1, false);
					var hits = searcher.Search(query, 5).ScoreDocs;
					totalHits = totalHits.Union(hits.Select(hit => $"{searcher.Doc(hit.Doc).GetField("question").GetStringValue(CultureInfo.CurrentCulture).LimitStringLength(100)} - /answer_{id}_{hit.Doc}")).ToList();
				}

				var result = totalHits.Count switch
				{
					0 => "Не найдено!",
					1 => totalHits.First(),
					_ => string.Join('\n', totalHits)
				};

				await _botClient.SendTextMessageAsync(senderID, result);
			}
#pragma warning disable CA1031
		} catch (Exception e)
#pragma warning restore CA1031
		{
			await OnUnhandledException(e);
			if (HasPermission(senderID, EPermission.User))
			{
				await _botClient.SendTextMessageAsync(message.Chat.Id, "Произошла неизвестная ошибка! О ней уже сообщено разработчику, а пока попробуйте ещё раз.");
			}
		}
	}

	private async Task HandleQuestionAdding(long senderID, string questionSetId, AddingType addingType)
	{
		if (!uint.TryParse(questionSetId, out var id) || !Program.Database.QuestionSets.TryGetValue(id, out var questionSet))
		{
			await _botClient.SendTextMessageAsync(senderID, "Неверный ID!");

			return;
		}

		if (questionSet.CreatorID != senderID)
		{
			await _botClient.SendTextMessageAsync(senderID, "Вы не имеете прав для изменения этого набора!");

			return;
		}

		while (true)
		{
			if (questionSet.Count > 1000)
			{
				await _botClient.SendTextMessageAsync(senderID, "Вы превысили лимит на количество вопросов для этого набора");

				return;
			}

			await _botClient.SendTextMessageAsync(
				senderID, addingType switch
				{
					AddingType.Single => "*Отправьте вопрос:*",
					AddingType.Separated => "Отправьте вопросы с ответами в формате `вопрос|ответ`, по вопросу на строчку:",
					AddingType.Multi => "*Отправьте вопрос:*\nДля отмены введите /abort",
					_ => throw new InvalidEnumArgumentException(nameof(addingType), (int) addingType, typeof(AddingType))
				}, parseMode: ParseMode.Markdown
			);

			var questionMessage = await WaitForMessage(senderID);

			if (string.IsNullOrEmpty(questionMessage?.Text))
			{
				await _botClient.SendTextMessageAsync(senderID, "Отменено.");

				return;
			}

			var inputQuestion = questionMessage.Text;

			switch (addingType)
			{
				case AddingType.Single:
				case AddingType.Multi:
				{
					var question = questionMessage.Text;

					await _botClient.SendTextMessageAsync(senderID, "Отправьте ответ:");
					var answerMessage = await WaitForMessage(senderID);

					if (answerMessage == null)
					{
						await _botClient.SendTextMessageAsync(senderID, "Отменено.");

						return;
					}

					var answer = answerMessage.Type == MessageType.Text
						? answerMessage.Text
						: answerMessage.Caption;

					var answerType = answerMessage.Type.ToString();

					questionSet.Add(
						question, answerType, answer ?? string.Empty,
						answerMessage.Type switch
						{
							MessageType.Photo when answerMessage.Photo != null => answerMessage.Photo[0].FileId,
							MessageType.Document when answerMessage.Document != null => answerMessage.Document!.FileId,
							_ => ""
						}
					);

					break;
				}
				case AddingType.Separated:
				{
					var lines = inputQuestion.Split('\n', StringSplitOptions.RemoveEmptyEntries);
					if (questionSet.Count + lines.Length > 1000)
					{
						await _botClient.SendTextMessageAsync(senderID, "Вы не можете добавить столько вопросов, поскольку превосходите лимит для этого набора");

						return;
					}

					foreach (var line in lines)
					{
						if (!line.Contains('|', StringComparison.Ordinal))
						{
							continue;
						}

						var question = line.Substring(0, line.IndexOf('|', StringComparison.Ordinal));
						var answer = line.Substring(line.IndexOf('|', StringComparison.Ordinal) + 1);
						questionSet.Add(question, answer);
					}

					break;
				}
			}

			await _botClient.SendTextMessageAsync(senderID, "Добавлено.");

			if (addingType != AddingType.Multi)
			{
				break;
			}
		}
	}

	private static bool HasPermission(long userID, EPermission permission) => (userID == developerID) || (permission <= EPermission.User);

	internal async Task OnUnhandledException(Exception exception)
	{
		await _botClient.SendTextMessageAsync(developerID, $"Exception occured! {exception}");
		await using var logFileStream = new FileStream("log.txt", FileMode.Open);
		await _botClient.SendDocumentAsync(developerID, new InputFileStream(logFileStream, "log.txt"));
	}

	internal async Task<bool> Start()
	{
		_botClient = new TelegramBotClient(Program.Config.BotToken);

		try
		{
			_botLogger.Debug("Testing API...");
			var testResult = await _botClient.TestApiAsync();

			if (!testResult)
			{
				_botLogger.Error("Токен некорректен!");

				return false;
			}
		} catch (ApiRequestException e)
		{
			_botLogger.Error("Ошибка при тестировании API!");
			_botLogger.Debug(e.ToString());

			return false;
		}

		_botClient.StartReceiving(HandleUpdate, HandleError, new ReceiverOptions {AllowedUpdates = [UpdateType.Message]}, _cancellationTokenSource.Token);

		return true;
	}

	private Task HandleError(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
	{
		_botLogger.Error(exception.ToString());
		return Task.CompletedTask;
	}

	internal void Stop()
	{
		_cancellationTokenSource.Cancel();
	}

	private async Task<Message?> WaitForMessage(long userID, uint timeoutInMinutes = 1)
	{
		if (_waitMessageStatuses.ContainsKey(userID))
		{
			return null;
		}

		_waitMessageStatuses.Add(userID, (new SemaphoreSlim(0, 1), null));
		await _waitMessageStatuses[userID].WaitSemaphore.WaitAsync(TimeSpan.FromMinutes(timeoutInMinutes));
		var message = _waitMessageStatuses[userID].ResultMessage;

		_waitMessageStatuses[userID].WaitSemaphore.Dispose();
		_waitMessageStatuses.Remove(userID);

		return message;
	}

	private enum AddingType
	{
		Single,
		Separated,
		Multi
	}

	private enum EPermission
	{
		User,
		Admin
	}

	public void Dispose()
	{
		_cancellationTokenSource.Dispose();
	}
}
